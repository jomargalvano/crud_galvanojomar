<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<div class="container mt-4">

<div class="HeadB"></div>
<span class="HeadF">Registration Interface</span>

<style>
.HeadB {
  width: 100%;
  height: 150px;
  background: rgb(119,136,153);
  opacity: 1;
  position: absolute;
  top: 1px;
  left: 0px;
  overflow: hidden;
}
.HeadF {
  width: 719px;
  color: rgba(0,0,0,1);
  position: absolute;
  top: 42px;
  left: -60px;
  font-family: Ramaraja;
  font-weight: Regular;
  font-size: 50px;
  opacity: 1;
  text-align: center;
}
</style>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div >
    <a href="<?php echo site_url('/add') ?>" class="btn btn-primary" style = "width: 1020px; height: 45px; left: 10px">Add New User</a>
</div>
  <div class="mt-3">
     <table class="table table-bordered" style = "width: 125%; margin-left: -150px;" id="users-list">
       <thead>
          <tr>
             <th style="width:10%"><center><font size = "3pt">FIRST NAME</center></th>
             <th style="width:10%"><center><font size = "3pt">LAST NAME</center></th>
             <th style="width:10%"><center><font size = "3pt">ADDRESS</center></th>
             <th style="width:10%"><center><font size = "3pt">EMAIL</center></th>
             <th style="width:10%"><center><font size = "3pt">MOBILE</center></th>
             <th style="width:10%"><center><font size = "3pt">GENDER</center></th>
             <th style="width:10%"><center><font size = "3pt">BIRTHDAY</center></th>
             <th style="width:10%"><center><font size = "3pt">EDIT</center></th>
             <th style="width:10%"><center><font size = "3pt">DELETE</center></th>
          </tr>
       </thead>
       <tbody>
          <?php if($users): ?>
          <?php foreach($users as $user): ?>
          <tr>
             <td class='text-left'><?php echo $user['FIRST_NAME']; ?></td>
             <td class='text-left'><?php echo $user['LAST_NAME']; ?></td>
             <td class='text-left'><?php echo $user['ADDRESS']; ?></td>
             <td class='text-left'><?php echo $user['EMAIL']; ?></td>
             <td class='text-left'><?php echo $user['MOBILE']; ?></td>
             <td class='text-left'><?php echo $user['GENDER']; ?></td>
             <td class='text-left'><?php echo $user['BIRTHDAY']; ?></td>
             <td class='text-center'>
             <span>
              <a href="<?php echo base_url('edit/'.$user['ID']);?>" class='btn btn-warning mr-3'><i class='fas fa-edit' style='font-size:15px'></i></a>
             </span>
             </td>
             <td class='text-center'>
             <span>
              <a href="<?php echo base_url('delete/'.$user['ID']);?>" class='btn btn-danger'><i class='fas fa-trash' style='font-size:15px'></i></a>
              </td>
             </span>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
     </table>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
      $('#users-list').DataTable();
  } );
</script>
</body>
</html>
