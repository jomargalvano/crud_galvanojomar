<!DOCTYPE html>
<html>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
   <style> .container {
      max-width: 500px;
    }

    .error {
      display: block;
      padding-top: 5px;
      font-size: 14px;
      color: red;
    }
  </style>
</head>

<body>
  <div class="container mt-5">

    <form method="post" id="update_user" name="update_user"
    action="<?= site_url('/update') ?>">
      <input type="hidden" name="id" id="id" value="<?php echo $user_obj['ID']; ?>">

      <div class="form-group">
        <label>FIRST NAME</label>
        <input type="text" name="FName" class="form-control" value="<?php echo $user_obj['FIRST_NAME']; ?>">
      </div>

      <div class="form-group">
        <label>LAST NAME</label>
        <input type="text" name="LName" class="form-control" value="<?php echo $user_obj['LAST_NAME']; ?>">
      </div>

      <div class="form-group">
        <label>ADDRESS</label>
        <input type="text" name="Address" class="form-control" value="<?php echo $user_obj['ADDRESS']; ?>">
      </div>

      <div class="form-group">
        <label>Email</label>
        <input type="email" name="Email" class="form-control" value="<?php echo $user_obj['EMAIL']; ?>">
      </div>

      <div class="form-group">
        <label>Mobile</label>
        <input type="text" name="Num" class="form-control" value="<?php echo $user_obj['MOBILE']; ?>">
      </div>

      <div class="form-group">
        <label>Gender</label>
        <select type="text" name="Sex" class="form-control" value="<?php echo $user_obj['GENDER']; ?>">
        <option>Male</option>
        <option>Female</option>
        </select>
      </div>

      <div class="form-group">
        <label>BirthDay</label>
        <input type="date" name="Bday" class="form-control" value="<?php echo $user_obj['BIRTHDAY']; ?>">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-warning">Change</button>
      </div>
    </form>
  </div>
</body>

</html>
