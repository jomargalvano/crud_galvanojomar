<!DOCTYPE html>
<html>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<style>

    .container {
      max-width: 500px;
    }

    .error {
      display: block;
      padding-top: 5px;
      font-size: 14px;
      color: red;
    }
  </style>
</head>

<body>
  <div class="container mt-5">
    <form method="post" id="add" name="add"
    action="<?= site_url('/submit-form') ?>">
      <div class="form-group">
        <label>First Name</label>
        <input type="text" name="FName" class="form-control" required>
      </div>

      <div class="form-group">
        <label>Last Name</label>
        <input type="text" name="LName" class="form-control" required>
      </div>

      <div class="form-group">
        <label>Address</label>
        <input type="text" name="Address" class="form-control" required>
      </div>

      <div class="form-group">
        <label>Email</label>
        <input type="text" name="Email" class="form-control" required>
      </div>

      <div class="form-group">
        <label>Mobile</label>
        <input type="text" name="Num" class="form-control" required>
      </div>

      <div class="form-group">
        <label>Gender</label>
        <select type="text" name="Sex" class="form-control" required>
        <option>Male</option>
        <option>Female</option>
        </select>
      </div>

      <div class="form-group">
        <label>Birthday</label>
        <input type="date" name="Bday" class="form-control">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-success">Add</button>
      </div>
    </form>
  </div>

</body>

</html>
