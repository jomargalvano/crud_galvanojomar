<?php
namespace App\Controllers;
use App\Models\CrudModel;
use CodeIgniter\Controller;

class CrudControllers extends Controller
{
    // show names list
    public function index(){
        $CrudModel = new CrudModel();
        $data['users'] = $CrudModel->orderBy('ID', 'DESC')->findAll();
        return view('table', $data);
    }

    // add name form
    public function create(){
        return view('add');
    }

    // add data
    public function store() {
        $CrudModel = new CrudModel();
        $data = [
            'FIRST_NAME' => $this->request->getVar('FName'),
            'LAST_NAME' => $this->request->getVar('LName'),
            'ADDRESS' => $this->request->getVar('Address'),
            'EMAIL' => $this->request->getVar('Email'),
            'MOBILE' => $this->request->getVar('Num'),
            'GENDER' => $this->request->getVar('Sex'),
            'BIRTHDAY'  => $this->request->getVar('Bday'),
        ];
        $CrudModel->insert($data);
        return $this->response->redirect(site_url('/table'));
    }

    // show single name
    public function singleUser($id = null){
        $CrudModel = new CrudModel();
        $data['user_obj'] = $CrudModel->where('ID', $id)->first();
        return view('edit', $data);
    }

    // update name data
    public function update(){
        $CrudModel = new CrudModel();
        $id = $this->request->getVar('ID');
        $data = [
          'FIRST_NAME' => $this->request->getVar('FName'),
          'LAST_NAME' => $this->request->getVar('LName'),
          'ADDRESS' => $this->request->getVar('Address'),
          'EMAIL' => $this->request->getVar('Email'),
          'MOBILE' => $this->request->getVar('Num'),
          'GENDER' => $this->request->getVar('Sex'),
          'BIRTHDAY'  => $this->request->getVar('Bday'),
        ];
        $CrudModel->update($id, $data);
        return $this->response->redirect(site_url('/table'));
    }

    // delete name
    public function delete($id = null){
        $CrudModel = new CrudModel();
        $data['user'] = $CrudModel->where('ID', $id)->delete($id);
        return $this->response->redirect(site_url('/table'));
    }
}
