<?php
namespace App\Models;
use CodeIgniter\Model;

class CrudModel extends Model
{
    protected $table = 'crud_registration';

    protected $primaryKey = 'ID';

    protected $allowedFields = ['FIRST_NAME', 'LAST_NAME', 'ADDRESS', 'EMAIL', 'MOBILE',
                                'GENDER', 'BIRTHDAY'];
}
